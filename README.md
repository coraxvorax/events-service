# Events service #

### Install ###

* using **java 8**

To start service you may

* execute command(Linux):

~~~
java -server -classpath $SERVICE_HOME/lib/*:$SERVICE_HOME/conf ru.events.EventsService
~~~

* using **docker**

You should write to file /$EVENTS_SERVICE_DIST/conf/config.main.yml parameter:

~~~
jsonFilePath: /app/data/data.json
~~~

You should execute command:

~~~
docker build -t events_service .
~~~

and

~~~
docker run --name events_service_dev -v /$EVENTS_SERVICE_DIST/conf/:/app/conf \
                                     -v /path/to/log/dir/:/LOGS  \
                                     -p 7883:7883 -p 10005:10005 \
                                     -d events_service
~~~

### Testing ###

To send event you should send empty **POST** request to:

~~~
http://localhost:7883/events/api/receive
~~~

For getting count of last events you should send **GET** request:

* for last **minutes**

~~~
http://localhost:7883/events/api/info/last/1/minutes
~~~

* for last **hours**

~~~
http://localhost:7883/events/api/info/last/1/hours
~~~

* for last **day**

~~~
http://localhost:7883/events/api/info/last/1/days
~~~

For example response in all success cases should be like:

~~~
{
    "event_count": 23
}
~~~

in case of error:

~~~
{
    "error": "Internal error"
}
~~~

### Technical information ###

The service logic located in the package **ru.events.service**