package ru.events.service;

import org.junit.Before;
import org.junit.Test;
import ru.events.util.EventTimestamp;

import static org.mockito.Mockito.*;

public class DefaultEventViewerTest {

    private EventStorage<FotoEvent> eventStorage;

    private DefaultEventViewer defaultEventViewer;

    @Before
    public void setUp() {
        eventStorage = mock(EventStorage.class);
        defaultEventViewer = new DefaultEventViewer(eventStorage);
    }

    @Test
    public void shouldGetEventCountForLastMinutesCorrectly() {
        defaultEventViewer.getEventCountForLastMinutes(1);
        verify(eventStorage, times(1)).getCountOfEvents(any(EventTimestamp.Range.class));
    }

    @Test
    public void shouldGetEventCountForLastHoursCorrectly() {
        defaultEventViewer.getEventCountForLastHours(1);
        verify(eventStorage, times(1)).getCountOfEvents(any(EventTimestamp.Range.class));
    }

    @Test
    public void shouldGetEventCountForLastDaysCorrectly() {
        defaultEventViewer.getEventCountForLastDay(1);
        verify(eventStorage, times(1)).getCountOfEvents(any(EventTimestamp.Range.class));
    }
}