package ru.events.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class DefaultEventReceiverTest {

    private EventStorage<FotoEvent> eventStorage;

    private DefaultEventReceiver defaultEventReceiver;

    @Before
    public void setUp() {
        eventStorage = mock(EventStorage.class);
        defaultEventReceiver = new DefaultEventReceiver(eventStorage);
    }

    @Test
    public void shouldSaveReceivedInStorageCorrectly() {
        final FotoEvent receivedEvent = new FotoEvent(UUID.randomUUID(), System.currentTimeMillis());
        defaultEventReceiver.receive(receivedEvent);
        final ArgumentCaptor<FotoEvent> eventCaptor = ArgumentCaptor.forClass(FotoEvent.class);

        verify(eventStorage, times(1)).saveEvent(eventCaptor.capture());

        final FotoEvent verifyingEvent = eventCaptor.getValue();

        assertThat(verifyingEvent, notNullValue());
        assertThat(receivedEvent, is(verifyingEvent));
    }
}