package ru.events.service;

import org.junit.Before;
import org.junit.Test;

import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListMap;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

public class DefaultEventStorageTest {

    private ConcurrentSkipListMap<Long, Queue<FotoEvent>> eventsCache;

    private DefaultEventStorage defaultEventStorage;

    @Before
    public void setUp() {
        eventsCache = new ConcurrentSkipListMap<>();
        defaultEventStorage = spy(new DefaultEventStorage(eventsCache));
    }

    @Test
    public void shouldSaveFotoEventWhenQueueIsAbsentForShiftKey() {
        defaultEventStorage.saveEvent(createFotoEvent());

        assertThat(eventsCache.size(), is(1));
    }

    @Test
    public void shouldSaveFotoEventWhenQueueIsPresentForShiftKey() {
        final Long shiftKey = 1111L;
        doReturn(shiftKey).when(defaultEventStorage).getShiftKeyOf(anyLong());
        insertEventIntoCacheWithKey(shiftKey);
        defaultEventStorage.saveEvent(createFotoEvent());

        assertThat(eventsCache.size(), is(1));
        assertThat(eventsCache.get(shiftKey).size(), is(2));
    }

    private FotoEvent createFotoEvent() {
        return new FotoEvent(UUID.randomUUID(), System.currentTimeMillis());
    }

    private void insertEventIntoCacheWithKey(Long shiftKey) {
        final Queue<FotoEvent> queue = new ConcurrentLinkedQueue<>();
        queue.offer(createFotoEvent());
        eventsCache.put(shiftKey, queue);
    }
}