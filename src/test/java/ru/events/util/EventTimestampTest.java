package ru.events.util;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;

public class EventTimestampTest {
    @Test
    public void shouldBeRangeStartLessThanRangeEndForLastMinutes() {
        final EventTimestamp.Range range = EventTimestamp.getRangeForLastDays(1);
        assertThat(range.getStart(), lessThan(range.getEnd()));
    }

    @Test
    public void shouldBeRangeStartLessThanRangeEndForLastHours() {
        final EventTimestamp.Range range = EventTimestamp.getRangeForLastHours(1);
        assertThat(range.getStart(), lessThan(range.getEnd()));
    }

    @Test
    public void shouldBeRangeStartLessThanRangeEndForLastDays() {
        final EventTimestamp.Range range = EventTimestamp.getRangeForLastDays(1);
        assertThat(range.getStart(), lessThan(range.getEnd()));
    }

    @Test
    public void shouldCreateCorrectShiftKey() {
        assertThat(EventTimestamp.createShiftKeyOf(11111111L), is(11111000L));
    }
}