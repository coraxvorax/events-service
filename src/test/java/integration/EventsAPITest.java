package integration;

import integration.rules.TestEventsServerRule;
import org.apache.http.client.fluent.Request;
import org.junit.ClassRule;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class EventsAPITest {

    @ClassRule
    public static final TestEventsServerRule SEARCH_SERVER_RULE = new TestEventsServerRule();

    @Test
    public void shouldSuccessSendRequest() throws Exception {
        assertThat(Request.Post(TestEventsServerRule.RECEIVE_URL)
                .addHeader("Test", "for tests")
                .addHeader("Test-user-agent", "test-agent")
                .execute()
                .returnResponse().getStatusLine().getStatusCode(), is(200));
    }

    //TODO: do tests
}
