package integration.rules;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import org.junit.rules.ExternalResource;
import ru.events.EventsService;
import ru.events.config.Configuration;
import ru.events.config.EventsConfiruration;
import ru.events.guice.EventsModule;
import ru.events.guice.meta.Default;
import ru.events.system.EventsServer;
import ru.events.system.jetty.WebServerException;

import java.util.EventListener;

import static org.mockito.Mockito.mock;

public class TestEventsServerRule extends ExternalResource {

    public final static String RECEIVE_URL = "http://localhost:7883/events/api/receive";
    public final static String GET_INFO_URL = "http://localhost:7883/events/api/info/last/1/hours";

    private EventsServer eventsServer;

    @Override
    protected void before() throws WebServerException {
        Injector searchServiceInjector = Guice.createInjector(
                new EventsModule(),
                new AbstractModule() {
                    @Override
                    protected void configure() {
                        final EventsConfiruration confiruration = mock(EventsConfiruration.class);
                        bind(Configuration.class)
                                .toInstance(confiruration);
                        bind(String.class)
                                .annotatedWith(Names.named("jetty.config"))
                                .toInstance("/jetty.xml");
                        bind(String.class)
                                .annotatedWith(Names.named("jetty.log.config"))
                                .toInstance("conf/jetty.log.xml");
                    }
                },
                new AbstractModule() {
                    @Override
                    protected void configure() {
                        bind(EventListener.class)
                                .annotatedWith(Default.class)
                                .to(EventsService.EventsListener.class);
                        bind(EventsServer.class)
                                .in(Singleton.class);
                    }
                }
        );

        eventsServer = searchServiceInjector.getInstance(EventsServer.class);
        eventsServer.configure();
        eventsServer.start();
    }

    @Override
    protected void after() {
        try {
            if (eventsServer != null) {
                eventsServer.stop();
            }
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }
}

