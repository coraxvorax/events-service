package ru.events;

import ch.qos.logback.access.servlet.TeeFilter;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import ru.events.config.Configuration;
import ru.events.config.EventsConfiruration;
import ru.events.guice.EventsModule;
import ru.events.guice.meta.Default;
import ru.events.guice.web.AbstractContextListener;
import ru.events.protocol.api.FotoEventAPI;
import ru.events.system.EventsServer;
import ru.events.system.jetty.WebServerException;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.EventListener;


public class EventsService {

    //TODO: add logger

    private EventsServer eventsServer;

    public static class EventsListener extends AbstractContextListener {

        @Inject
        private Configuration configuration;

        @Override
        protected APIBuilder buildAPI() throws InstantiationException, IllegalAccessException {
            return new APIBuilder()
                    .servlet()
                    .basePath("/events/*")
                    .filter("/events/*", new TeeFilter())
                    .set(FotoEventAPI.class)
                    .setParam(JSONConfiguration.FEATURE_POJO_MAPPING, "true")
                    .setParam(
                            ResourceConfig.PROPERTY_MEDIA_TYPE_MAPPINGS,
                            String.format("xml:%s,json:%s",
                                    MediaType.APPLICATION_XML_TYPE,
                                    MediaType.APPLICATION_JSON_TYPE
                            )
                    ).buildServlet();
        }
    }

    public void init() {
        try {
            final Injector eventServiceInjector = Guice.createInjector(
                    new EventsModule(),
                    new AbstractModule() {
                        @Override
                        protected void configure() {
                            try {
                                bind(Configuration.class)
                                        .toInstance(new EventsConfiruration(
                                                Paths.get(ClassLoader.getSystemResource("config.main.yml")
                                                        .toURI())));
                            } catch (IOException | URISyntaxException e) {
                                //TODO: add log
                            }
                            bind(String.class)
                                    .annotatedWith(Names.named("jetty.config"))
                                    .toInstance("/jetty.xml");
                            try {
                                bind(String.class)
                                        .annotatedWith(Names.named("jetty.log.config"))
                                        .toInstance(Paths.get(ClassLoader.getSystemResource("jetty.log.xml").toURI())
                                                .toAbsolutePath().toString());
                            } catch (URISyntaxException e) {
                                //TODO: add log
                            }
                        }
                    },
                    new AbstractModule() {
                        @Override
                        protected void configure() {
                            bind(EventListener.class)
                                    .annotatedWith(Default.class)
                                    .to(EventsListener.class);
                        }
                    }
            );

            eventsServer = eventServiceInjector.getInstance(EventsServer.class);
            eventsServer.configure();
            eventsServer.start();
        } catch (Exception e) {
            //TODO: add log
        }
    }

    //TODO: do stop as hook
    public void stop() {
        try {
            eventsServer.stop();
        } catch (WebServerException e) {
            //TODO: add log
        }
    }

    public static void main(String[] args) {
        final EventsService searcherService = new EventsService();
        searcherService.init();
    }
}