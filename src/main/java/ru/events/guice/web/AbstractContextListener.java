package ru.events.guice.web;


import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.google.inject.servlet.CustomGuiceFilter;
import com.google.inject.servlet.GuiceServletContextListener;
import com.google.inject.servlet.ServletModule;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class AbstractContextListener extends GuiceServletContextListener {

    @Inject
    private Injector injector;

    @Override
    protected Injector getInjector() {
        final APIBuilder builder;
        try {
            builder = buildAPI();
            final List<Module> modules = builder.getModules();
            modules.add(CustomGuiceFilter.staticInjectionModule());
            return injector.createChildInjector(modules.toArray(new Module[modules.size()]));
        } catch (InstantiationException | IllegalAccessException e) {
            //TODO: log or ignore
            e.printStackTrace();
        }
        return null;
    }

    protected abstract APIBuilder buildAPI() throws InstantiationException, IllegalAccessException;

    public class APIBuilder {

        private List<Module> modules = new ArrayList<>();

        public ServletModuleBuilder servlet() {
            return new ServletModuleBuilder(this);
        }

        public List<Module> getModules() {
            return new ArrayList<>(modules);
        }

    }


    public class ServletModuleBuilder {
        private List<Class> endpoints = new ArrayList<>();
        private List<FilterInfo> filters = new ArrayList<>();
        private List<Class> singletons = new ArrayList<>();
        private Map<String, String> params = new HashMap<>();
        private String basePath = "/*";
        private final APIBuilder builder;

        public ServletModuleBuilder(APIBuilder builder) {
            this.builder = builder;
        }

        public ServletModuleBuilder set(Class endpoint)
                throws IllegalAccessException, InstantiationException {
            params.put(PackagesResourceConfig.PROPERTY_PACKAGES, endpoint.getPackage().getName());
            endpoints.add(endpoint);
            return this;
        }

        private char[] getBasePath() {
            return basePath.toCharArray();
        }

        public ServletModuleBuilder basePath(String basePath) {
            this.basePath = basePath;
            return this;
        }

        public ServletModuleBuilder setParam(String key, String value) {
            params.put(key, value);
            return this;
        }

        public ServletModuleBuilder withSingleton(Class singleton) {
            singletons.add(singleton);
            return this;
        }

        public ServletModuleBuilder jerseyFilterRequest(Class clz) {
            params.put(ResourceConfig.PROPERTY_CONTAINER_REQUEST_FILTERS, clz.getCanonicalName());
            return this;
        }

        public ServletModuleBuilder jerseyFilterResponse(Class clz) {
            params.put(ResourceConfig.PROPERTY_CONTAINER_RESPONSE_FILTERS, clz.getCanonicalName());
            return this;
        }

        public ServletModuleBuilder filter(String route, Filter filter) {
            filters.add(new FilterInfo(route, filter));
            return this;
        }

        public ServletModuleBuilder filter(String route, Class filter) {
            //TODO: do
            //filters.add(new FilterInfo(route, filter));
            return this;
        }

        private Map<String, String> getParams() {
            return new HashMap<>(params);
        }

        public APIBuilder buildServlet() {
            builder.modules.add(new ServletModule() {
                @Override
                protected void configureServlets() {
                    try {
                        endpoints.stream().forEach(this::bind);
                        singletons.stream().forEach(s -> bind(s).in(Singleton.class));
                        filters.stream().forEach(fi -> filter(fi.getRoute()).through(fi.getFilter()));
                        serve(new String(getBasePath())).with(new GuiceContainer(injector), getParams());
                    } catch (Exception ignore) {
                    }
                }
            });
            return builder;
        }

        public APIBuilder buildJerseyServlet() {
            builder.modules.add(new JerseyServletModule() {
                @Override
                protected void configureServlets() {
                    try {
                        endpoints.stream().forEach(this::bind);
                        singletons.stream().forEach(s -> bind(s).in(Singleton.class));
                        filters.stream().forEach(fi -> filter(fi.getRoute()).through(fi.getFilter()));
                        serve(new String(getBasePath())).with(new GuiceContainer(injector), getParams());
                    } catch (Exception ignore) {
                    }
                }
            });
            return builder;
        }

        public ServletModuleBuilder withJerseyPackage(String jerseyPackage) {
            String packages = params.get(PackagesResourceConfig.PROPERTY_PACKAGES);
            if (packages != null) {
                params.put(PackagesResourceConfig.PROPERTY_PACKAGES, packages + "," + jerseyPackage);
            } else {
                params.put(PackagesResourceConfig.PROPERTY_PACKAGES, jerseyPackage);
            }
            return this;
        }
    }

    private class FilterInfo {
        private final String route;
        private final Filter filter;

        private FilterInfo(String route, Filter filter) {
            this.route = route;
            this.filter = filter;
        }

        public String getRoute() {
            return route;
        }

        public Filter getFilter() {
            return filter;
        }
    }
}
