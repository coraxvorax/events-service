package ru.events.guice;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import ru.events.service.*;

import java.util.Queue;
import java.util.concurrent.ConcurrentSkipListMap;


public class EventsModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(new TypeLiteral<EventReceiver<FotoEvent>>() {
        }).to(DefaultEventReceiver.class);

        bind(new TypeLiteral<EventViewer<FotoEvent>>() {
        }).to(DefaultEventViewer.class);

        bind(new TypeLiteral<ConcurrentSkipListMap<Long, Queue<FotoEvent>>>() {
        }).toInstance(new ConcurrentSkipListMap<>());

        bind(new TypeLiteral<EventStorage<FotoEvent>>() {
        }).to(DefaultEventStorage.class);

    }
}
