package ru.events.service;

import ru.events.util.EventTimestamp;

import javax.inject.Inject;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListMap;

public class DefaultEventStorage implements EventStorage<FotoEvent> {

    //TODO: add logs

    private final ConcurrentSkipListMap<Long, Queue<FotoEvent>> eventsCache;

    @Inject
    public DefaultEventStorage(ConcurrentSkipListMap<Long, Queue<FotoEvent>> eventsCache) {
        this.eventsCache = eventsCache;
    }

    @Override
    public void saveEvent(FotoEvent event) {
        final Long timestamp = event.getTimestampCreation();
        final Long shiftKey = getShiftKeyOf(timestamp);

        eventsCache.computeIfPresent(shiftKey, (k, q) -> {
            q.offer(event);
            return q;
        });
        eventsCache.computeIfAbsent(shiftKey, (k) -> {
            final Queue<FotoEvent> queue = new ConcurrentLinkedQueue<>();
            queue.offer(event);
            return queue;
        });
    }

    protected Long getShiftKeyOf(Long timestamp) {
        return EventTimestamp.createShiftKeyOf(timestamp);
    }

    @Override
    public long getCountOfEvents(EventTimestamp.Range range) {
        return eventsCache.subMap(range.getStart(), true, range.getEnd(), true)
                .entrySet().stream().mapToInt(e -> e.getValue().size()).sum();
    }
}
