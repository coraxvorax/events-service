package ru.events.service;

import java.util.UUID;

public class FotoEvent implements Comparable<FotoEvent> {
    private final UUID fotoID;
    private final Long timestampCreation;

    public FotoEvent(UUID fotoID, Long timestampCreation) {
        this.fotoID = fotoID;
        this.timestampCreation = timestampCreation;
    }

    public UUID getFotoID() {
        return fotoID;
    }

    public Long getTimestampCreation() {
        return timestampCreation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FotoEvent fotoEvent = (FotoEvent) o;

        if (!fotoID.equals(fotoEvent.fotoID)) {
            return false;
        }
        return timestampCreation.equals(fotoEvent.timestampCreation);

    }

    @Override
    public int hashCode() {
        int result = fotoID.hashCode();
        result = 31 * result + timestampCreation.hashCode();
        return result;
    }

    @Override
    public int compareTo(FotoEvent o) {
        if (this.timestampCreation < o.timestampCreation) {
            return -1;
        } else if (this.timestampCreation.equals(o.timestampCreation)) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public String toString() {
        return "FotoEvent{" +
                "fotoID=" + fotoID +
                ", timestampCreation=" + timestampCreation +
                '}';
    }
}
