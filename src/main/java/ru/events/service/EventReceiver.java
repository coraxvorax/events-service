package ru.events.service;

public interface EventReceiver<T> {
    void receive(T fotoEvent);
}
