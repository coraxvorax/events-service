package ru.events.service;

import ru.events.util.EventTimestamp;

public interface EventStorage<T> {
    void saveEvent(T event);

    long getCountOfEvents(EventTimestamp.Range range);
}
