package ru.events.service;

import ru.events.util.EventTimestamp;

import javax.inject.Inject;

public class DefaultEventViewer implements EventViewer<FotoEvent> {

    private final EventStorage<FotoEvent> eventStorage;

    //TODO: add TimestampUtil

    @Inject
    public DefaultEventViewer(EventStorage<FotoEvent> eventStorage) {
        this.eventStorage = eventStorage;
    }

    @Override
    public long getEventCountForLastMinutes(Integer countMinutes) {
        return eventStorage.getCountOfEvents(EventTimestamp.getRangeForLastMinutes(countMinutes));
    }

    @Override
    public long getEventCountForLastHours(Integer countHours) {
        return eventStorage.getCountOfEvents(EventTimestamp.getRangeForLastHours(countHours));
    }

    @Override
    public long getEventCountForLastDay(Integer countDays) {
        return eventStorage.getCountOfEvents(EventTimestamp.getRangeForLastDays(countDays));
    }
}
