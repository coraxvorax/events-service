package ru.events.service;

public interface EventViewer<T> {
    long getEventCountForLastMinutes(Integer countMinutes);

    long getEventCountForLastHours(Integer countHours);

    long getEventCountForLastDay(Integer countDays);
}
