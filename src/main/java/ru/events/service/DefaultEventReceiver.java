package ru.events.service;

import javax.inject.Inject;

public class DefaultEventReceiver implements EventReceiver<FotoEvent> {

    private final EventStorage<FotoEvent> eventStorage;

    @Inject
    public DefaultEventReceiver(EventStorage<FotoEvent> eventStorage) {
        this.eventStorage = eventStorage;
    }

    @Override
    public void receive(FotoEvent fotoEvent) {
        eventStorage.saveEvent(fotoEvent);
    }
}
