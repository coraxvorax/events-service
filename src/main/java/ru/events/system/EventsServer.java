package ru.events.system;

import org.eclipse.jetty.server.Server;
import ru.events.guice.meta.Default;
import ru.events.system.jetty.WebServerException;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.EventListener;

public class EventsServer extends ConfiguredServer {

    @Inject
    public EventsServer(@Named("jetty.config") String config,
                        @Named("jetty.log.config") String logConfig,
                        @Default EventListener eventListener)
            throws WebServerException {
        super(config, eventListener, logConfig);
    }

    @Override
    protected void initSessionIdManager(Server server) {
        //NOP
    }
}
