package ru.events.system.jetty;

import ch.qos.logback.access.jetty.RequestLogImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.SessionIdManager;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.xml.XmlConfiguration;

import java.util.EventListener;


public abstract class JettyWebServer implements WebServer {

    private static final Log LOG = LogFactory.getLog(JettyWebServer.class);

    private Server server;
    private ServletContextHandler ctxHandler;
    private SessionIdManager sessionIdManager;

    public JettyWebServer(String configFile) throws WebServerException {
        this(configFile, "conf/jettylog/logback.main.xml");
    }

    public JettyWebServer(String configFile, String logConfig) throws WebServerException {
        init(configFile, logConfig);
    }

    private void init(String configFile, String logConfig) throws WebServerException {
        try {
            System.out.println("configFile = [" + configFile + "], logConfig = [" + logConfig + "]");
            server = (Server) new XmlConfiguration(Resource.newSystemResource(configFile)
                    .getInputStream()).configure();

            final Handler parent = server.getHandler();
            if (parent instanceof HandlerCollection) {
                for (Handler handler : ((HandlerCollection) parent).getHandlers()) {
                    if (handler instanceof ContextHandlerCollection) {
                        ctxHandler = new ServletContextHandler((ContextHandlerCollection) handler, "/", ServletContextHandler.SESSIONS);
                    }
                }
            }

            setLog(logConfig);

            LOG.info("JettyWebServer initialized");
        } catch (Exception e) {
            LOG.error(e, e);
            throw new WebServerException(e);
        }
    }

    public void initSessionManager() {
        initSessionIdManager(server);
    }

    protected abstract void initSessionIdManager(Server server);

    private void setLog(String logConfig) {
        RequestLogImpl log =
                new RequestLogImpl();
        log.setFileName(logConfig);
        log.start();
        server.setRequestLog(log);
    }

    public void start() throws WebServerException {
        try {
            server.start();
        } catch (Exception e) {
            LOG.error(e, e);
            throw new WebServerException(e);
        }

        LOG.info("JettyWebServer started");
    }

    public void stop() throws WebServerException {
        try {
            server.stop();
        } catch (Exception e) {
            LOG.error(e, e);
        }
        LOG.info("JettyWebServer stopped");
    }

    protected ServletContextHandler getServletContext() {
        return ctxHandler;
    }

    public void addEventListener(EventListener listener) {
        ctxHandler.addEventListener(listener);
    }
}
