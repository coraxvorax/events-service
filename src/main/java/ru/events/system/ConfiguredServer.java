package ru.events.system;

import com.google.inject.servlet.CustomGuiceFilter;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import ru.events.system.jetty.JettyWebServer;
import ru.events.system.jetty.WebServerException;

import java.util.EventListener;


public class ConfiguredServer extends JettyWebServer {

    public ConfiguredServer(String config,
                            EventListener eventListener,
                            String logConfig) throws WebServerException {
        super(config, logConfig);
        getServletContext().addEventListener(eventListener);
    }

    public void configure() throws WebServerException {
        getServletContext().addFilter(CustomGuiceFilter.class, "/*", null);
        getServletContext().addServlet(DefaultServlet.class, "/");
    }

    @Override
    protected void initSessionIdManager(Server server) {
        //NOP
    }
}
