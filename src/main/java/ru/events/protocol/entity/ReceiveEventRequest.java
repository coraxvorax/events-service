package ru.events.protocol.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReceiveEventRequest {

    @JsonProperty("timestamp")
    private Long receiveTimestamp = System.currentTimeMillis();

    public ReceiveEventRequest() {
    }

    public ReceiveEventRequest(Long receiveTimestamp) {
        this.receiveTimestamp = receiveTimestamp;
    }

    public Long getReceiveTimestamp() {
        return receiveTimestamp;
    }

    @Override
    public String toString() {
        return "ReceiveEventRequest{" +
                "receiveTimestamp=" + receiveTimestamp +
                '}';
    }
}
