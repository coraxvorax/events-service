package ru.events.protocol.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EventsCountInfo {

    @JsonProperty("event_count")
    private Long eventCount;

    public EventsCountInfo() {
    }

    public EventsCountInfo(Long eventCount) {
        this.eventCount = eventCount;
    }

    public Long getEventCount() {
        return eventCount;
    }

    @Override
    public String toString() {
        return "EventsInfo{" +
                "eventCount=" + eventCount +
                '}';
    }
}
