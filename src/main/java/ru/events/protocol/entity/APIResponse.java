package ru.events.protocol.entity;

import com.fasterxml.jackson.annotation.JsonProperty;


public class APIResponse {

    public static DefaultErrorResponse ofError(String error) {
        return new DefaultErrorResponse(error);
    }


    public static class DefaultErrorResponse {
        @JsonProperty("error")
        private String error;

        public DefaultErrorResponse(String error) {
            this.error = error;
        }
    }
}
