package ru.events.protocol;

import ru.events.protocol.entity.APIResponse;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


public class APIException extends WebApplicationException {

    private static final long serialVersionUID = 5839433685349492344L;

    public APIException() {
        super(Response.status(Response.Status.INTERNAL_SERVER_ERROR).build());
    }

    public APIException(String message) {
        super(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(APIResponse.ofError(message))
                .type(MediaType.APPLICATION_JSON)
                .build());
    }
}
