package ru.events.protocol.api;

import ru.events.protocol.APIException;
import ru.events.protocol.entity.EventsCountInfo;
import ru.events.service.EventReceiver;
import ru.events.service.EventViewer;
import ru.events.service.FotoEvent;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Path("api")
public class FotoEventAPI {

    //TODO: add parameters validation

    @Inject
    private EventReceiver<FotoEvent> eventEventReceiver;

    @Inject
    private EventViewer<FotoEvent> eventViewer;

    @POST
    @Path("receive")
    @Produces({MediaType.APPLICATION_JSON})
    public Response receiveEvent() {
        try {
            eventEventReceiver.receive(
                    new FotoEvent(UUID.randomUUID(), System.currentTimeMillis())
            );
            return Response.ok().build();
        } catch (Exception e) {
            throw new APIException("Internal error");
        }
    }

    @GET
    @Path("info/last/{count}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getEventsCount(@PathParam("count") Integer countMinutes) {
        try {
            return Response.ok(new EventsCountInfo(
                    eventViewer.getEventCountForLastMinutes(countMinutes)))
                    .build();
        } catch (Exception e) {
            throw new APIException("Internal error");
        }
    }

    @GET
    @Path("info/last/{count}/minutes")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getEventsCountForLastMinutes(@PathParam("count") Integer countMinutes) {
        try {
            return Response.ok(new EventsCountInfo(
                    eventViewer.getEventCountForLastMinutes(countMinutes)))
                    .build();
        } catch (Exception e) {
            throw new APIException("Internal error");
        }
    }

    @GET
    @Path("info/last/{count}/hours")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getEventsInfoForLastHour(@PathParam("count") Integer countHours) {
        try {
            return Response.ok(new EventsCountInfo(
                    eventViewer.getEventCountForLastHours(countHours)))
                    .build();
        } catch (Exception e) {
            throw new APIException("Internal error");
        }
    }

    @GET
    @Path("info/last/{count}/days")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getEventsInfoForLastDay(@PathParam("count") Integer countDays) {
        try {
            return Response.ok(new EventsCountInfo(
                    eventViewer.getEventCountForLastDay(countDays)))
                    .build();
        } catch (Exception e) {
            throw new APIException("Internal error");
        }
    }
}
