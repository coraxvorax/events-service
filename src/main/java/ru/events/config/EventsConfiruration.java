package ru.events.config;

import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;

public class EventsConfiruration implements Configuration {

    private final MappedConfig config;

    public EventsConfiruration(Path configPath) throws IOException, URISyntaxException {
        try (InputStream in = Files.newInputStream(configPath)) {
            this.config = new Yaml().loadAs(in, MappedConfig.class);
        }
    }

    @Override
    public String getPrefixServletName() {
        return config.prefixServletName;
    }

    public static class MappedConfig {
        private String prefixServletName;

        public String getPrefixServletName() {
            return prefixServletName;
        }

        public void setPrefixServletName(String prefixServletName) {
            this.prefixServletName = prefixServletName;
        }
    }
}

